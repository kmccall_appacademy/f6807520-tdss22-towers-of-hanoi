# Towers of Hanoi
#
# Write a Towers of Hanoi game:
# http://en.wikipedia.org/wiki/Towers_of_hanoi
#
# In a class `TowersOfHanoi`, keep a `towers` instance variable that is an array
# of three arrays. Each subarray should represent a tower. Each tower should
# store integers representing the size of its discs. Expose this instance
# variable with an `attr_reader`.
#
# You'll want a `#play` method. In a loop, prompt the user using puts. Ask what
# pile to select a disc from. The pile should be the index of a tower in your
# `@towers` array. Use gets
# (http://andreacfm.com/2011/06/11/learning-ruby-gets-and-chomp.html) to get an
# answer. Similarly, find out which pile the user wants to move the disc to.
# Next, you'll want to do different things depending on whether or not the move
# is valid. Finally, if they have succeeded in moving all of the discs to
# another pile, they win! The loop should end.
#
# You'll want a `TowersOfHanoi#render` method. Don't spend too much time on
# this, just get it playable.
#
# Think about what other helper methods you might want. Here's a list of all the
# instance methods I had in my TowersOfHanoi class:
# * initialize
# * play
# * render
# * won?
# * valid_move?(from_tower, to_tower)
# * move(from_tower, to_tower)
#
# Make sure that the game works in the console. There are also some specs to
# keep you on the right track:
#
# ```bash
# bundle exec rspec spec/towers_of_hanoi_spec.rb
# ```
#
# Make sure to run bundle install first! The specs assume you've implemented the
# methods named above.


class TowersOfHanoi
  attr_reader :towers

  def initialize
    @towers = [[3, 2, 1], [], []]
  end

  def play
    until won?
      system('clear')
      puts show_towers
      get_move
    end
    congratulate

  end

  def get_move
    print "Which tower would you like to move from?"
    from_input = gets.chomp.to_i
    print "Which tower would you like to move to?"
    to_input = gets.chomp.to_i

    if valid_move?(from_input, to_input)
      move(from_input, to_input)
    else
      give_valid_move
    end

  end

  def give_valid_move
    print "That's not a valid move, try again!\n"
    get_move
  end

  def move (from_tower, to_tower)
    from = towers[from_tower]
    to = towers[to_tower]

    disc = from.pop
    to.push(disc)

  end

  def valid_move?(from_tower, to_tower)
    from = towers[from_tower]
    to = towers[to_tower]
    return false if from.empty?
    return false if !to.empty? && from.last > to.last
    true
  end

  def won?
    return true if towers[1].length == 3 || towers[2].length == 3

    false
  end

  def show_towers
    top = towers.map { |tower| tower.length == 3 ? "#{tower.last}" : " " }
    mid = towers.map { |tower| tower.length >= 2 ? "#{tower[1]}" : " " }
    bottom = towers.map { |tower| tower.length >= 1 ? "#{tower[0]}" : " " }

    "#{top.join(" ")}\n#{mid.join(" ")}\n#{bottom.join(" ")}\n_ _ _ \n0 1 2"
  end

  def congratulate
    if won?
    system('clear')
    puts show_towers
    puts "WELL PLAYED...YOU WIN!!!"
    end
  end
end



if __FILE__ == $PROGRAM_NAME
  t = TowersOfHanoi.new
  t.play
end
